# SimplonDelivery : Exercice de programmation fonctionnelle en Typescript

Application en ligne de commande en TypeScript qui permet à l'utilisateur de convertir des montants entre l'euro (EUR), le dollar canadien (CAD) et le yen japonais (JPY), ainsi que de calculer les frais de livraison et les frais de douane des colis.

## Configuration
Configuration d'un projet Node : https://www.julienrollin.com/posts/node-typescript-starter/

## Execution dans un terminal
1. Cloner le projet sur votre machine
2. Aller dans le dossier du projet
3. Executer les commandes suivantes :
- npm run build
- node dist
4. Entrer les données demandées