import { get } from "http";

// Module pour lire les inputs et outputs donnés (ligne par ligne) 
const readline = require('readline');

// Readline fournit des méthodes pour les interactions avec le user
// Création d'une interface pour les inputs (standard input) et outputs (standard output)
let rl = readline.createInterface(
                    process.stdin, 
                    process.stdout
);

//utilisation de la methode question()
console.log('1. Convertisseur de devises')
rl.question('Montant : ', (montantAConvertir: string) => {

    // Methode question() prend en parametre la question '' et un callback avec un seul argument = la réponse sous forme de string
    // On convertit cette  string en nombre
    const montantIn: number = parseFloat(montantAConvertir);

    rl.question("Devise d'entrée : EUR, CAD ou JPY : ", (deviseEntrée: string) => {

        rl.question('Devise de sortie : EUR, CAD ou JPY : ', (deviseSortie: string) => {
        
            let montantOut: number = 0;
        
            let tauxDeChange : number[][] = [
                [1, 1.5, 130],   // Taux de EUR [0] vers CAD [1], JPY [2]
                [0.67, 1, 87],   // Taux de CAD [1] vers EUR [0], JPY [2]
                [0.0077, 0.0115, 1] // Taux de JPY [2] vers EUR [0], CAD [1]
            ];

            // On a les taux de change de chaque devise (pour une valeur standard de 1), 
            // on peut utiliser les inputs pour accéder aux différents tableaux

            function trouverTauxDeChange(devise: string) {
                if (devise === 'EUR') {
                    return 0; // qui correspond au tableau 1, index 0 du tabeau tauxDeChange (tauxDeChange[0]), tableau de conversion d'1 EUR vers CAD et JPY
                    // mais marche aussi pour trouver le taux de sortie tauxDeChange[1][0] correspond a une conversion du CAD [1] vers l'EUR [0]
                } else if (devise=== 'CAD') {
                    return 1; // qui correspond au tableau 2, index 1 du tabeau : tauxDeChange[1]
                } else if (devise === 'JPY') {
                    return 2; // qui correspond au tableau 3, index 2 du tabeau : tauxDeChange[2]
                } else {
                    console.log("devise non reconnue");
                    return 3;
                }
            }

            function convertirDevise(montantIn: number, deviseIn: string, deviseOut: string){

                let tauxDeChangeIn: number = trouverTauxDeChange(deviseIn);
                let tauxDeChangeOut: number = trouverTauxDeChange(deviseOut);
                montantOut = montantIn * tauxDeChange[tauxDeChangeIn][tauxDeChangeOut];
                return montantOut

            } 
            if (montantIn > 0) {
                convertirDevise(montantIn, deviseEntrée, deviseSortie)
                console.log('Conversion : ' + montantIn + ' ' + deviseEntrée + ' = ' + montantOut + ' ' + deviseSortie)
            } else {
                console.log("Le montant doit être supérieur à 0")
            }
            
            console.log('')
            console.log('///////////////////////////////////////////////////////////////////////////////')
            console.log('')
            console.log('2. Calculateur de frais de livraison')
            rl.question('Pays de destination de la livraison (EUR, CAD ou JPY) : ', (paysDeDestination: string) => {
            
                rl.question('Poids du colis (kg) : ', (poidsDuColis: string) => {
            
                    rl.question('Dimensions (format : L,l,h (cm)) : ', (dimensionsColis: string) => {
            
                        let paysOut: string = paysDeDestination;
                        let poids: number = parseFloat(poidsDuColis);
                        // 1. on split la chaine L, l, h en tableau (de string) avec methode split()
                        // 2. On boucle sur chaque element e du tableau avec methode map()
                        // 3. Synthax : array.map(function(currentValue, index, arr), thisValue) sur chaque e, on applique la methode de conversion en nombre parseFloat()
                        // 4. D'une string, on otient un tableau de nombres
                        let dimensions: number[] = dimensionsColis.split(',').map((e) => parseFloat(e));
                        let tarifAuPoidsParDevise: number[][] = [
                            [10, 15, 1000], // Jusqu'à 1 kg : 10 EUR, 15 CAD, 1000 JPY
                            [20, 30, 2000], // Entre 1 et 3 kg : 20 EUR, 30 CAD, 2000 JPY
                            [30, 45, 3000]  // Plus de 3 kg : 30 EUR, 45 CAD, 3000 JPY
                        ]
            
                        function getIndexDestination(paysOut: string) {
                            if (paysOut === 'EUR') {
                                return 0;
                            } else if (paysOut === 'CAD') {
                                return 1;
                            } else if (paysOut === 'JPY') {
                                return 2;
                            } else {
                                console.log('Destination non valide')
                                return 3;
                            }
                        }
            
                        function calculerSupplement(dimensions: number[], paysOut: string) {
                            // methode reduce() parcourt le tableau et utilise un callback sur chaque element
                            // callback avec 2 arguments : accumulateur stock le résulat de l'opération (addition) pour chaque element nombre parcouru
                            let dimensionFinale: number = dimensions.reduce((accumulateur, nombre) => accumulateur + nombre)
                            let paysDestination: string = paysOut;
            
                            if (dimensionFinale > 150) {
                                if (paysDestination === 'EUR' ) {
                                    return 5;
                                } else if (paysDestination === 'CAD' ) {
                                    return 7.5;
                                } else if (paysDestination === 'JPY' ) {
                                    return 500;
                                } else {
                                    console.log('Destination non valide')
                                    return 0;
                                }
                            } else {
                                return 0;
                            }
                        }
            
                        function getIndexTarifsParPoids(poids: number) {
                                        
                            if (poids <= 1) {
                                return 0;
                            } else if (poids > 1 && poids <= 3) {
                                return 1;
                            } else if (poids > 3) {
                                return 2;
                            } else {
                                console.log('Poids non valide')
                                return 3;
                            }
                        }
                        
                        function calculerTarif(paysOut: string, poids: number, dimensions: number[]) {
                            
                            let supplement: number = calculerSupplement(dimensions, paysOut);
                            let tarifsParPoids: number = getIndexTarifsParPoids(poids)
                            let paysDestination: number = getIndexDestination(paysOut);
            
                            return tarifAuPoidsParDevise[tarifsParPoids][paysDestination] + supplement;
                        }
            
                        let fraisDeLivraisons = calculerTarif(paysOut, poids, dimensions);
                        console.log('Frais de livraison : ' + fraisDeLivraisons + ' ' + paysOut)
                        console.log('')
                        console.log('///////////////////////////////////////////////////////////////////////////////')
                        console.log('')
                        console.log('3. Calcul de frais de douanes') 

                        console.log('Colis à destination de : ' + paysOut)
                        rl.question('Valeur du colis : ', (valeurDuColis: string) => {

                            function calculerFraisDeDouane(paysOut: string, valeurDuColis: string) {
                                let valeur: number = parseFloat(valeurDuColis);
                                let destination: string = paysOut;
                                
                                if (destination === 'CAD' && valeur > 20) {
                                    return valeur * 0.15;
                                } else if (destination === 'JPY' && valeur > 5000) {
                                    return valeur * 0.10;
                                } else {
                                    console.log('Aucun frais de douane')
                                    return 0;
                                }
                            }

                            let fraisDeDouane: number = calculerFraisDeDouane(paysOut, valeurDuColis)
                            console.log('Frais de douanes : ' + fraisDeDouane + ' ' + paysOut)
                        });
                    });
                });
            });
        });
    });
});